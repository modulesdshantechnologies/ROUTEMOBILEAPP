electionapp.controller("AchievementDetailsCtrl1",function($scope,AchievementDetailsServices1,$ionicModal){
//   var achievementId = $stateParams.achievementId;
//   console.log('achievementId +++++++++++++++++ ', achievementId);
   $scope.achievement = {};
   $scope.deleteAchievementId = null;

// Get all achievements
     $scope.getAllAchievement = function ()
     {
          AchievementDetailsServices1.getAllAchievement().then(function (res)
          {
             $scope.achievementList = res.data;
          });
     };

 //get saved achievements details
     $scope.getAchievement = function (achievementId) {
              AchievementDetailsServices1.getAchievement(achievementId).then(function (res) {
                  $scope.achievement = res.data;
//                   $scope.images = res.data.AchievementMoreImages;

              });
         };

 //   save all achievements
     $scope.saveDetails=function()
     {
         AchievementDetailsServices1.postAllAchievementDetails($scope.achievement)
             .then(function (resultDetails)
             {
                  $scope.getAllAchievement();
                 console.log('Achievement detail saved successfully');
             },
              function error(err)
             {
                 console.log('Achievement save failed:', err);
             });
     };

// delete the achievements
     $scope.deleteAchievement = function(achievementId)
     {
         AchievementDetailsServices1.deleteAchievement(achievementId)
             .then(function(res)
             {
                 $scope.getAllAchievement();
                 console.log('Achievement deleted.');
             },
             function error(err)
             {
                console.log('Achievement delete failed:', err);
             });
     }
// update the achievements

$scope.updateAchievement = function(){
        AchievementDetailsServices1.updateAchievement($scope.achievement)
            .then(function(res){
                $scope.getAllAchievement();
                console.log('Achievement updated.');
            },
            function error(err) {
               console.log('Achievement update failed:', err);
            });
    }

$scope.setSlideImages = function(images){
    $scope.slideImages = images;
}


$scope.showAddForm = function(){
        $scope.achievement = {};
        $scope.formTitle = 'Add New Achievement';
        $scope.showAddBtn =  true;
        $scope.showEditbtn = false;
    };

    $scope.showEditForm = function(){
        $scope.formTitle = 'Update Achievement';
        $scope.showAddBtn =  false;
        $scope.showEditbtn = true;
    }

    $scope.openDeleteModal = function(achievementId){
        console.log(achievementId);
        $scope.deleteAchievementId = achievementId;
    };

$scope.files = [];
$scope.upload=function(){
 var file=$scope.myFile;
//  console.log($scope.files);
console.log(file)
//  console.log(file.name);
//    alert($scope.files.length+" files selected ... Write your Upload Code");
 var filess = $('#uploadBtn')[0].files[0]
         console.log(filess)
        fileUpload.uploadFileToUrl(filess).then(function (response) {
        alert("image uploaded")
        console.log(response.data);
        var filedata=response.data;
        if (filedata){
        AchievementDetailsServices1.updateMoreImages(imagearrayid,filedata,{fileObj:file.base64}).then(function(response){
        console.log(response.data);
        })

        }

});

};

$scope.uploadfile=function(fliedata)
{
console.log(fliedata);
console.log("i am here")
var data;
data=fliedata.base64;
//console.log(data);
AchievementDetailsServices1.setProductImages(data);

}
var imagearrayid;
$scope.uploadMoreImages=function(id)
{
console.log(id);
imagearrayid=id;
$("#uploadBtn").click();
}

$scope.callinguploadfile=function()
{
alert("i am not a");
console.log("somethiggggggggggggggggggggggg");
$("#upload").click();
}




$scope.data = {};
//$scope.grids =[
//        {src:'img/events1.jpg',details:'George Feldenkreis leaves Cuba with one-year-old son, Oscar, a pregnant wife and only $700. Faced with the immediate need to earn a living, he begins importing everything from auto parts to apparel.'},
//        {src:'img/events2.jpg',details:'George travels to the Far East to develop key manufacturing relationships that pave the way for Supreme International, a business manufacturing school uniforms and 4-pocket linen Guayabera shirts that earns him the title: “King of Guayaberas.”'},
//        {src:'img/events3.jpg',details:'Oscar Feldenkreis joins the company, using his keen fashion sense to transition the business from a private label distributor to a branded sportswear manufacturer and distributor.'},
//        {src:'img/events4.jpg',details:'Launches its first major brand, Natural Issue®, specializing in reactive printing.'},
//        {src:'img/events5.jpg',details:'Initial public offering NASDAQ: SUPI.'},
//        {src:'img/events6.jpg',color:"bluebg",id:6,details:'Acquisition of Munsingwear® and Grand Slam® brands adds 110 years of history and product innovation to the company’s heritage.'}
//        ];

  $ionicModal.fromTemplateUrl('modal.html', function(modal) {
    $scope.gridModal = modal;
  }, {
    scope: $scope,
    animation: 'slide-in-up'
  });
// open video modal
  $scope.openModal = function(image) {
    $scope.data.selected = image;
    $scope.gridModal.show();
  };
// close video modal
  $scope.closeModal = function() {
    $scope.gridModal.hide();
  };
  //Cleanup the video modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.gridModal.remove();
  });

    electionapp.directive('gridImage', function(){
      return function($scope, element, attrs){
          var url = attrs.gridImage;
          element.css({
              'background-image': 'url(' + url +')',
          });
      };
  });






//unwanted codes
  $scope.getAllAchievement();
});
