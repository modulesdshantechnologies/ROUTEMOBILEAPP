electionapp.controller("EventDetailsCtrl",function($scope,EventDetailsServices,$ionicModal,$stateParams){
    var eventId = $stateParams.eventId;
    console.log('event Id++++++++++++++++',eventId);
    $scope.event = {};
    $scope.deleteEventId = null;

// Get all events
     $scope.getAllEvent = function ()
     {
          EventDetailsServices.getAllEvent().then(function (res)
          {
             $scope.eventList = res.data;
             console.log($scope.eventList);
          });
     };

 //get saved event details
     $scope.getEvent = function (eventId) {
              EventDetailsServices.getEvent(eventId).then(function (res) {
//                  $scope.event = res.data;
             $scope.images = res.data.EventMoreImages;

              });
         };

 //   save all events
     $scope.saveDetails=function()
     {

         $scope.event.EventBrochures = EventServices.getEventBrochuresImages();
         console.log($scope.event);
         EventDetailsServices.postAllEventDetails($scope.event)
             .then(function (resultDetails)
             {
                  $scope.getAllEvent();
                 console.log('Event detail saved successfully');
             },
              function error(err)
             {
                 console.log('Event save failed:', err);
             });
     };

// delete the events
     $scope.deleteEvent = function(eventId)
     {
         EventDetailsServices.deleteEvent(eventId)
             .then(function(res)
             {
                 $scope.getAllEvent();
                 console.log('Event deleted.');
             },
             function error(err)
             {
                console.log('Event delete failed:', err);
             });
     }
// update the events

$scope.updateEvent = function(){
        EventDetailsServices.updateEvent($scope.event)
            .then(function(res){
                $scope.getAllEvent();
                console.log('Event updated.');
            },
            function error(err) {
               console.log('Event update failed:', err);
            });
    }

$scope.setSlideImages = function(images){
    $scope.slideImages = images;
}

$scope.showAddForm = function(){
        $scope.event = {};
        $scope.formTitle = 'Add New Event';
        $scope.showAddBtn =  true;
        $scope.showEditbtn = false;
    };

    $scope.showEditForm = function(){
        $scope.formTitle = 'Update Event';
        $scope.showAddBtn =  false;
        $scope.showEditbtn = true;
    }

    $scope.openDeleteModal = function(eventId){
        console.log(eventId);
        $scope.deleteEventId = eventId;
    };


//fileupload
$scope.files = [];
$scope.upload=function(){
 var file=$scope.myFile;
//  console.log($scope.files);
console.log(file)
//  console.log(file.name);
//    alert($scope.files.length+" files selected ... Write your Upload Code");
 var filess = $('#uploadBtn')[0].files[0]
         console.log(filess)
        fileUpload.uploadFileToUrl(filess).then(function (response) {
        alert("image uploaded")
        console.log(response.data);
        var filedata=response.data;
        if (filedata){
        EventDetailsServices.updateMoreImages(imagearrayid, filedata, {fileObj:file.base64}).then(function(response){
        console.log(response.data);
        })

        }

});

};

//$scope.uploadfile=function(fliedata)
//{
//console.log(fliedata);
//console.log("i am here")
////var data;
//data=fliedata.base64;
////console.log(data);
//EventServices.setProductImages(data);
//
//}
var imagearrayid;
$scope.uploadMoreImages=function(id)
{
console.log(id);
imagearrayid=id;
$("#uploadBtn").click();
}

$scope.callinguploadfile=function()
{
alert("i am not a");
console.log("somethiggggggggggggggggggggggg");
$("#upload").click();
}


$scope.uploadfile=function(fliedata)
{
//console.log(fliedata);
//console.log(fliedata);
//console.log("i am here")
//var data;
data=fliedata.base64;
//console.log(data);
EventDetailsServices.setEventBrochuresImages(data);
}

$scope.data = {};
/*
$scope.grids =[
        {src:'img/events1.jpg',details:'George Feldenkreis leaves Cuba with one-year-old son, Oscar, a pregnant wife and only $700. Faced with the immediate need to earn a living, he begins importing everything from auto parts to apparel.'},
        {src:'img/events2.jpg',details:'George travels to the Far East to develop key manufacturing relationships that pave the way for Supreme International, a business manufacturing school uniforms and 4-pocket linen Guayabera shirts that earns him the title: “King of Guayaberas.”'},
        {src:'img/events3.jpg',details:'Oscar Feldenkreis joins the company, using his keen fashion sense to transition the business from a private label distributor to a branded sportswear manufacturer and distributor.'},
        {src:'img/events4.jpg',details:'Launches its first major brand, Natural Issue®, specializing in reactive printing.'},
        {src:'img/events5.jpg',details:'Initial public offering NASDAQ: SUPI.'},
        {src:'img/events6.jpg',color:"bluebg",id:6,details:'Acquisition of Munsingwear® and Grand Slam® brands adds 110 years of history and product innovation to the company’s heritage.'}
        ];
*/

  $ionicModal.fromTemplateUrl('modal.html', function(modal) {
    $scope.gridModal = modal;
  }, {
    scope: $scope,
    animation: 'slide-in-up'
  });
// open video modal
  $scope.openModal = function(image) {
    $scope.data.selected = image;
    $scope.gridModal.show();
  };
// close video modal
  $scope.closeModal = function() {
    $scope.gridModal.hide();
  };
  //Cleanup the video modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.gridModal.remove();
  });

    electionapp.directive('gridImage', function(){
      return function($scope, element, attrs){
          var url = attrs.gridImage;
          element.css({
              'background-image': 'url(' + url +')',
          });
      };
  });



//angular.element( document.querySelector('.upload') ).change(function() {
// $timeout(function () {
//      $scope.upload();
//    }, 500);
//
//});

  $scope.getEvent(eventId);

//unwanted codes
});
