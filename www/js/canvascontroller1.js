electionapp.controller("CanvasDetailsCtrl1",function($scope,CanvasDetailsServices1,$ionicModal){
//    var canvasId = $stateParams.canvasId;
//    console.log('Canvas Id+++++++++++++++++ ',canvasId);
    $scope.canvas = {};
    $scope.deleteCanvasId = null;
    // Get all canvas
    $scope.getAllCanvas = function () {
         CanvasDetailsServices1.getAllCanvas().then(function (res) {
             $scope.canvasList = res.data;
         });
    };

    // Get Canvas by id
    $scope.getCanvas = function (canvasId) {
         CanvasDetailsServices1.getCanvas(canvasId).then(function (res) {
             $scope.canvas = res.data;
//             $scope.images = res.data.CanvasMoreImages;
             //console.log($scope.canvas);
         });
    };

    // save a Canvas
    $scope.saveDetails=function(){
        CanvasDetailsServices1.postAllCanvasDetails($scope.canvas)
            .then(function (resultDetails) {
                 $scope.getAllCanvas();
                console.log('Canvas detail saved successfully');
             },
             function error(err) {
                console.log('Canvas save failed:', err);
             });
    };

    // Delete a canvas
    $scope.deleteCanvas = function(canvasId){
        CanvasDetailsServices1.deleteCanvas(canvasId)
            .then(function(res){
                $scope.getAllCanvas();
                console.log('Canvas deleted.');
            },
            function error(err) {
               console.log('Canvas delete failed:', err);
            });
    }

    // Update a canvas
    $scope.updateCanvas = function(){
        CanvasDetailsServices1.updateCanvas($scope.canvas)
            .then(function(res){
                $scope.getAllCanvas();
                console.log('Canvas updated.');
            },
            function error(err) {
               console.log('Canvas update failed:', err);
            });
    }

    $scope.showAddForm = function(){
        $scope.canvas = {};
        $scope.formTitle = 'Add New Canvas';
        $scope.showAddBtn =  true;
        $scope.showEditbtn = false;
    };

    $scope.showEditForm = function(){
        $scope.formTitle = 'Update Canvas';
        $scope.showAddBtn =  false;
        $scope.showEditbtn = true;
    }

    $scope.openDeleteModal = function(canvasId){
        console.log(canvasId);
        $scope.deleteCanvasId = canvasId;
    };


$scope.setSlideImages = function(images){
    $scope.slideImages = images;
}


$scope.files = [];
$scope.upload=function(){
 var file=$scope.myFile;

 var filess = $('#uploadBtn')[0].files[0]
        fileUpload.uploadFileToUrl(filess).then(function (response) {
        alert("image uploaded")
        console.log(response.data);
        var filedata=response.data;
        if (filedata){
        console.log(filedata,imagearrayid);
        CanvasDetailsServices1.updateMoreImages(imagearrayid, filedata, {fileObj:file.base64}).then(function(response){
        console.log(response.data);
        })

        }

});

};

$scope.uploadfile=function(fliedata)
{
console.log(fliedata);
console.log("i am here")
var data;
data=fliedata.base64;
//console.log(data);
CanvasDetailsServices1.setProductImages(data);

}
var imagearrayid;
$scope.uploadMoreImages=function(id)
{
console.log(id);
imagearrayid=id;
$("#uploadBtn").click();
}

$scope.callinguploadfile=function()
{
alert("i am not a");
console.log("somethiggggggggggggggggggggggg");
$("#upload").click();
}

$scope.data = {};
/*
$scope.grids =[
        {src:'img/events1.jpg',details:'George Feldenkreis leaves Cuba with one-year-old son, Oscar, a pregnant wife and only $700. Faced with the immediate need to earn a living, he begins importing everything from auto parts to apparel.'},
        {src:'img/events2.jpg',details:'George travels to the Far East to develop key manufacturing relationships that pave the way for Supreme International, a business manufacturing school uniforms and 4-pocket linen Guayabera shirts that earns him the title: “King of Guayaberas.”'},
        {src:'img/events3.jpg',details:'Oscar Feldenkreis joins the company, using his keen fashion sense to transition the business from a private label distributor to a branded sportswear manufacturer and distributor.'},
        {src:'img/events4.jpg',details:'Launches its first major brand, Natural Issue®, specializing in reactive printing.'},
        {src:'img/events5.jpg',details:'Initial public offering NASDAQ: SUPI.'},
        {src:'img/events6.jpg',color:"bluebg",id:6,details:'Acquisition of Munsingwear® and Grand Slam® brands adds 110 years of history and product innovation to the company’s heritage.'}
        ];
*/

  $ionicModal.fromTemplateUrl('modal.html', function(modal) {
    $scope.gridModal = modal;
  }, {
    scope: $scope,
    animation: 'slide-in-up'
  });
// open video modal
  $scope.openModal = function(image) {
    $scope.data.selected = image;
    $scope.gridModal.show();
  };
// close video modal
  $scope.closeModal = function() {
    $scope.gridModal.hide();
  };
  //Cleanup the video modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.gridModal.remove();
  });

    electionapp.directive('gridImage', function(){
      return function($scope, element, attrs){
          var url = attrs.gridImage;
          element.css({
              'background-image': 'url(' + url +')',
          });
      };
  });

//angular.element( document.querySelector('.upload') ).change(function() {
// $timeout(function () {
//      $scope.upload();
//    }, 500);
//
//});

    // init
// unwanted code

$scope.getAllCanvas();

});
