// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic','starter.controllers','ngMaterial','ngCordova','ionic-datepicker','ionic-toast','ionic-timepicker','pdf','chart.js'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})




.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider

.state('route', {
            url: '/route',
            templateUrl: 'templates/route.html',
            controller:'IndexCtrl'
       })

.state('login', {
             url: '/login',
             templateUrl: 'templates/login.html',
             controller:'IndexCtrl'
        })
.state('register', {
             url: '/register',
             templateUrl: 'templates/register.html',
        })
.state('userMapDetails', {
             url: '/userMapDetails/:mapId',
             templateUrl: 'templates/userMapDetails.html',
             controller:'userCtrl'
        })

.state('routeedit', {
             url: '/routeedit/:mapId',
             templateUrl: 'templates/routeedit.html',
             controller:'routeeditCtrl'
        })

.state('search', {
             url: '/search',
             templateUrl: 'templates/search.html',
             controller:'searchCtrl'
         })
.state('searchdetails', {
             url: '/searchdetails',
             templateUrl: 'templates/searchdetails.html',
             controller:'searchCtrl'
         })




  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/register');
});
