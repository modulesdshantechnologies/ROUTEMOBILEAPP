electionapp.factory('indexServices',function($http){
var baseUrl='http://localhost:3063'

    var postAllLoginDetails=function(data)
    {
    return $http.post(baseUrl+'/RouterLogin',data);
    }

    var postAllRegisterDetails = function(settingdata)
    {
    console.log(settingdata);
     return $http.post(baseUrl+'/Usermaps',settingdata);
    }

    var getAllMapDetails = function()
     {
         return $http.get(baseUrl+'/allMapDetails');
     }

     var getSingleMapDetails=function(mapId)
      {
         return $http.get(baseUrl+'/mapBymongoId/'+ mapId);
      }

      var updateMapDetails =function(editdata)
     {
        return $http.post(baseUrl+'/editmapBymongoId',editdata);
     }
     var deleteMap=function(mapId)
             {
                return $http.delete(baseUrl+'/mapBymongoId/'+mapId);
             }
     var getAllUsersDetails = function()
          {
              return $http.get(baseUrl+'/allRouteDetails');
          }


       return{
      postAllLoginDetails:postAllLoginDetails,
      postAllRegisterDetails:postAllRegisterDetails,
      getAllMapDetails: getAllMapDetails,
      getSingleMapDetails: getSingleMapDetails,
      updateMapDetails: updateMapDetails,
      deleteMap: deleteMap,
      getAllUsersDetails: getAllUsersDetails
}

});
